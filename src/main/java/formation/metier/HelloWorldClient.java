package formation.metier;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import interfaces.IEndPoint;
import interfaces.ILivreEndPoint;
import metier.Auteur;
import metier.Livre;



public class HelloWorldClient {
	
	
	    public static void main(String[] args) throws MalformedURLException, RemoteException {
	        /*URL url = new URL("http://localhost:9998/ws/hello?wsdl");
	        QName qname = new QName("http://Metier.Formation/", "HelloWorldService");
	        Service service = Service.create(url, qname);

	        IHelloWorld hello = service.getPort(IHelloWorld.class);
	        System.out.println(hello.getHelloWorldAsString("mathieu"));*/
	    	
	    	URL url = new URL("http://localhost:9998/ws/auteur?wsdl");
	        QName qname = new QName("http://endPoint/", "AuteurEndPointService");
	        Service service = Service.create(url, qname);
	        
	        URL url2 = new URL("http://localhost:9998/ws/livre?wsdl");
	        QName qname2 = new QName("http://endPoint/", "LivreEndPointService");
	        Service service2 = Service.create(url2, qname2);

	        IEndPoint endPointauteur = service.getPort(IEndPoint.class);
	        ILivreEndPoint endPointLivre = service2.getPort(ILivreEndPoint.class);
	        
	        //AUTEUR
	        //endPointauteur.createAuteur(4, "rogers", "jimmy");
	        //endPointauteur.deleteAuteur(1);
	        //endPointauteur.updateAuteur(1, "rachid" , "eren");
	        
	        
	        
	        //LIVRE
	        //endPointLivre.createLivre(2, "superSympa", 2);
	        //endPointLivre.deleteLivre(1);
	    	//endPointLivre.updateTitreLivre(1, "attack on titan" );
	        //endPointLivre.addAuteurToLivre(1, 2);
	        //endPointLivre.removeAuteurToLivre(1, 2);
	        System.out.println(endPointLivre.readAllLivre());
	       
	    	
	      System.out.println("fait");
	        
	    }

}
